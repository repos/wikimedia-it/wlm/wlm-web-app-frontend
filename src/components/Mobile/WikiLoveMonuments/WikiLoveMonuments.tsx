import styles from './WikiLoveMonuments.module.css'
import { ReactComponent as WikiLogo } from '../../../assets/wiki-primary.svg'
import { ReactComponent as Close } from '../../../assets/close.svg'
import { useTranslation } from 'react-i18next'
import { usePaeseContext } from '../../../context/PaeseContext'

interface Props {
  infoWiki: boolean
  setInfoWiki: (infoWiki: boolean) => void
  setPresentazione: (presentazione: boolean) => void
}


const countries = [
  {
    name: 'Italia',
    mail: 'contatti@wikilovesmonuments.it',
    sito: 'https://www.wikimedia.it/wiki-loves-monuments/',
  },
  {
    name: 'Ireland',
    mail: 'info@wikimedia.ie',
    sito: 'https://commons.wikimedia.org/wiki/Commons:Wiki_Loves_Monuments_2024_in_Ireland',
  }
]

export default function WikiLoveMonuments({
  infoWiki,
  setInfoWiki,
  setPresentazione,
}: Props) {
  const { i18n, t } = useTranslation()
  const { country } = usePaeseContext()
  return (
    <div
      className={styles.WikiLoveMonuments}
      style={{
        opacity: infoWiki ? '1' : '0',
        pointerEvents: infoWiki ? 'all' : 'none',
        transition: 'all 0.5s ease-in-out',
      }}
    >
      <div className={styles.TopWikiLove}>
        <div className={styles.WikiLoveLabel}>
          <WikiLogo />
          <span className="ms-2">{t('WIKI_LOVES_MONUMENTS')}</span>
        </div>
        <div className="pointer">
          <Close onClick={() => setInfoWiki(false)} />
        </div>
      </div>
      <div className="mt-4">
        {i18n.language === 'it' ? (
          <div className={styles.TextWikiLove}>
            <p>
              <strong>Ecco la webapp di Wiki Loves Monuments!</strong>
            </p>
            <p>
              {' '}
              Puoi utilizzare questo strumento per partecipare alle edizioni
              italiana e irlandese di Wiki Loves Monuments, che si svolge dal 1° al 30
              settembre. Prima di partecipare, ricordati di leggere il{' '}
              <a
                href="https://www.wikimedia.it/news/wiki-loves-monuments-italia-2023-regolamento/"
                target={'_blank'}
              >
                regolamento del concorso
              </a>
              .
            </p>{' '}
            <p>
              Wiki Loves Monuments è{' '}
              <strong>il più grande concorso fotografico al mondo</strong>. Il
              suo obiettivo è coinvolgere le persone nella{' '}
              <strong>documentazione del patrimonio culturale</strong>{' '}
              con immagini pubblicate con licenza libera, che potranno essere
              utilizzate anche per illustrare le voci di{' '}
              <strong>Wikipedia, l’enciclopedia libera</strong>.
            </p>
            <p>
              Se non lo hai già fatto, per partecipare al concorso e caricare le
              tue fotografie devi prima{' '}
              <a
                href="https://commons.wikimedia.org/wiki/Main_Page"
                target="_blank"
              >
                registrarti a Wikimedia Commons
              </a>
              . L’utenza creata è valida anche su Wikipedia.
            </p>
            <p>
              Tutte le fotografie caricate per il concorso, anche tramite questa
              webapp, sono pubblicate con la <strong>licenza</strong>{' '}
              <a
                href="https://creativecommons.org/licenses/by-sa/4.0/deed.it"
                target="_blank"
              >
                CC BY-SA 4.0
              </a>
              , che ne consente il libero riutilizzo, per qualsiasi scopo,
              purché venga mantenuta la medesima licenza e ne vengano
              riconosciuti autori e autrici originali.
            </p>{' '}
            <p>
              Puoi utilizzare questa webapp anche al di fuori del concorso Wiki
              Loves Monuments, in qualsiasi momento. Utilizza i filtri per
              cercare i monumenti di tuo interesse, scatta le foto (o recuperale
              dal tuo archivio) e caricale.
            </p>{' '}
            <p>
              Visita il sito{' '}
              <a
                href={countries.find((c) => c.name === country.label)?.sito}
                target={'_blank'}
              >
                {countries.find((c) => c.name === country.label)?.sito}
              </a>{' '}
              per scoprire tutti i dettagli del concorso. Per qualsiasi domanda
              puoi scrivere a{' '}
              <a
                href={`mailto:${countries.find((c) => c.name === country.label)?.mail}`}
              >
                {countries.find((c) => c.name === country.label)?.mail}
              </a>
              .
            </p>
            
          </div>
        ) : (
          <div className={styles.TextWikiLove}>
            <p>
              <strong>Here is the Wiki Loves Monuments webapp!</strong>
            </p>
            <p>
              {' '}
              You can use this tool to participate in the Italian and Irish editions of
              Wiki Loves Monuments, which takes place from 1 to 30 September.
              Before participating,{' '}
              <a
                href="https://www.wikimedia.it/news/wiki-loves-monuments-italia-2023-regolamento/"
                target={'_blank'}
              >
                remember to read the competition rules
              </a>
              .
            </p>{' '}
            <p>
              Wiki Loves Monuments is{' '}
              <strong>the largest photo contest in the world</strong>. Its goal
              is to involve people in the documentation of the cultural
              heritage with images published with a free license, which can also
              be used to illustrate the entries of{' '}
              <strong>Wikipedia, the free encyclopedia</strong>.
            </p>
            <p>
              If you have not already done so, to participate in the contest and
              upload your photos you must first{' '}
              <a
                href="https://commons.wikimedia.org/wiki/Main_Page"
                target="_blank"
              >
                register on Wikimedia Commons
              </a>
              . The user created is also valid on Wikipedia.
            </p>
            <p>
              All the photos uploaded for the contest, also through this webapp,
              are published with the <strong>license</strong>{' '}
              <a
                href="https://creativecommons.org/licenses/by-sa/4.0/deed.it"
                target="_blank"
              >
                CC BY-SA 4.0
              </a>
              , which allows their free reuse, for any purpose, provided that
              the same license is maintained and the original authors are
              recognized.
            </p>{' '}
            <p>
              You can use this webapp even outside the Wiki Loves Monuments
              contest, at any time. Use the filters to search for the monuments
              of your interest, take the photos (or retrieve them from your
              archive) and upload them.
            </p>{' '}
            <p>
              Visit the site{' '}
              <a
                href={countries.find((c) => c.name === country.label)?.sito}
                target={'_blank'}
              >
                {countries.find((c) => c.name === country.label)?.sito}
              </a>{' '}
              to discover all the details of the competition. For any questions
              you can write to{' '}
              <a
                href={`mailto:${countries.find((c) => c.name === country.label)?.mail}`}
              >
                {countries.find((c) => c.name === country.label)?.mail}
              </a>
              .
            </p>
          </div>
        )}
        <div
          className={styles.ButtonRivediTutorial}
          onClick={() => {
            setInfoWiki(false)
            setPresentazione(true)
          }}
        >
          <strong>
          {t('WATCH_THE_APP_TUTORIAL_AGAIN')}
          </strong>
        </div>
      </div>
    </div>
  )
}

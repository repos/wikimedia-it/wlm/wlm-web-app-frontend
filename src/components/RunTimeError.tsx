import { useTranslation } from 'react-i18next'
import Layout from './Mobile/Layout'

export default function RunTimeError({ error }: { error: Error }) {
  const { t } = useTranslation()
  return (
    <Layout>
      <div className="text-center p-4">
        <div>
          {t('AN_ERROR_HAS_OCCURRED_PLEASE_RELOAD_THE_PAGE')}
        </div>
      </div>
    </Layout>
  )
}

import { createContext, useContext, useEffect, useState } from 'react'
import { Geocontext } from '../types'
import { useGeocontexts } from '../hooks/geocontext'

interface ContextTop {
  geoPermission?: string
  geocontexts: Geocontext[]
  //setGeoPermission?: (geoPermission: string) => void
}

const TopStateContext = createContext<ContextTop>({
  geocontexts: [],
})

export function TopContextProvider({
  children,
}: {
  children: React.ReactNode
}) {
  const { data: geocontexts } = useGeocontexts()

  //geo perms
  const [geoPermission, setGeoPermission] = useState<string>('prompt')

  useEffect(() => {
    if (navigator?.permissions?.query) {
      navigator.permissions
        .query({ name: 'geolocation' })
        .then((permissionStatus) => {
          console.log(
            `geolocation permission status is ${permissionStatus.state}`
          )
          setGeoPermission(permissionStatus.state)

          permissionStatus.onchange = () => {
            console.log(
              `geolocation permission status has changed to ${permissionStatus.state}`
            )
            setGeoPermission(permissionStatus.state)
          }
        })
    } else {
      setGeoPermission('prompt')
    }
  }, [])



  return (
    <TopStateContext.Provider
      value={{
        geoPermission,
        geocontexts: geocontexts ?? [],
      }}
    >
      {children}
    </TopStateContext.Provider>
  )
}


export function useTopContextState() {
  const filters = useContext(TopStateContext)
  return filters
}

import { useQuery } from '@tanstack/react-query'
import { API_URL } from '../const'
import { Comune } from '../types'
import axios from 'axios'
import { serializeQueryParams } from './utils'

export async function getComuni(
  geocontext: number | undefined,
  params: Record<string, any> = {},
  signal?: AbortSignal
) {
  return (
    await axios.get(`${API_URL}/geo-contexts/${geocontext}/municipalities/`, {
      signal,
      params: serializeQueryParams(params),
    })
  ).data as Comune[]
}

export function useComuni(geocontext: number | undefined, params: Record<string, any> = {}) {
  return useQuery(
    ['comuni', geocontext, params],
    ({ signal }) => getComuni(geocontext!, params, signal),
    {
      keepPreviousData: true,
      suspense: false,
      enabled: !!geocontext && (!!params.q || !!params.id)
    }
  )
}
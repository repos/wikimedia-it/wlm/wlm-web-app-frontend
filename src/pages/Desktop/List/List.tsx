import classNames from 'classnames'
import { Fragment, Suspense, useEffect, useMemo, useRef, useState } from 'react'
import BlockFilters from '../../../components/Desktop/BlockFilters'
import BlockOrdering from '../../../components/Desktop/BlockOrdering'
import { ReactComponent as Search } from '../../../assets/search.svg'
import { ReactComponent as Camera } from '../../../assets/camera.svg'
import { ReactComponent as CloseSecondary } from '../../../assets/close-secondary.svg'
import { ReactComponent as Map } from '../../../assets/map.svg'
import { ReactComponent as ListIcon } from '../../../assets/list.svg'
import { ReactComponent as MapActive } from '../../../assets/map-active.svg'
import { ReactComponent as ListIconActive } from '../../../assets/list-active.svg'
import Layout from '../../../components/Desktop/Layout'
import { useQsFilters } from '../../../hooks/filters'
import styles from './List.module.css'
import { useInfiniteMomuments } from '../../../hooks/monuments'
import { Waypoint } from 'react-waypoint'
import IconMonument from '../../../components/IconMonument'
import { Link, useLocation, useNavigate, useParams } from 'react-router-dom'
import Detail from '../../Mobile/Detail'
import { getActiveGeocontext, getLabelFromSlug, parseSmartSlug } from '../../../utils'
import { useTranslation } from 'react-i18next'
import { useTopContextState } from '../../../context/TopContext'
import { usePaeseContext } from '../../../context/PaeseContext'
import { useAuthUser } from 'use-eazy-auth'

const getFilters = (params: URLSearchParams) => ({
  search: params.get('search') ?? '',
  municipality: params.get('municipality') ?? '',
  ordering: params.get('ordering') ?? '',
  in_contest: params.get('in_contest') ?? 'true',
  only_without_pictures: params.get('only_without_pictures') ?? '',
  app_category: params.get('app_category') ?? '',
  user_lat: params.get('user_lat') ?? '',
  user_lon: params.get('user_lon') ?? '',
  monument_id: params.get('monument_id') ?? '',
  monument_lat: params.get('monument_lat') ?? '',
  monument_lon: params.get('monument_lon') ?? '',
  cluster_active: params.get('cluster_active') ?? '',
  geo_context: params.get('geo_context') ?? '',
  only_disabled: params.get('only_disabled') ?? '',
  without_coordinates: params.get('without_coordinates') ?? '',
})

interface Props {
  filters: {
    search: string
    municipality: string
    ordering: string
    app_category: string
    in_contest: string
    only_without_pictures: string
    user_lat: string
    user_lon: string
    monument_lat: string
    monument_lon: string
    monument_id: string
    geo_context: string
    only_disabled: string
    without_coordinates: string
  }
  setDetail: (monument: number | null) => void
  detail: number | null
  setFilters: (filters: any) => void
}

export function ListMonuments({
  filters,
  setDetail,
  detail,
  setFilters,
}: Props) {
  const filtersForMonument = useMemo(() => {
    return {
      search: filters.search,
      municipality: filters.municipality,
      ordering: filters.ordering,
      app_category: filters.app_category,
      in_contest: filters.in_contest,
      only_without_pictures: filters.only_without_pictures,
      user_lat: filters.user_lat,
      user_lon: filters.user_lon,
      geo_context: filters.geo_context,
      only_disabled: filters.only_disabled,
      without_coordinates: filters.without_coordinates
    }
  }, [filters])

  const enableQuery = useMemo(() => {
    if (filters.ordering === '') {
      return false
    }
    return (
      filters.ordering !== 'distance' ||
      (filters.ordering === 'distance' &&
        !!filters.user_lat &&
        !!filters.user_lon)
    )
  }, [filters])

  const {
    data: infiniteMonuments,
    hasNextPage,
    isFetchingNextPage,
    isLoading,
    isFetching,
    fetchNextPage,
  } = useInfiniteMomuments(filtersForMonument, enableQuery)

  useEffect(() => {
    if (history.state?.scroll) {
      listMonumentsRef.current!.scrollTop = history.state.scroll
    } else {
      listMonumentsRef.current!.scrollTop = 0
    }
  }, [])

  const [isLoadingPosition, setIsLoadingPosition] = useState(false)

  const listMonumentsRef = useRef<HTMLDivElement>(null)

  useEffect(() => {
    if (filters.monument_id !== '') {
      setDetail(Number(filters.monument_id))
    } else {
      setDetail(null)
    }
  }, [filters.monument_id])

  const { geoPermission } = useTopContextState()

  function success(position: any) {
    const latitude = position.coords.latitude
    const longitude = position.coords.longitude
    setFilters({
      ...filters,
      ordering: 'distance',
      user_lat: latitude,
      user_lon: longitude,
    })
    setIsLoadingPosition(false)
  }

  function error() {
    setIsLoadingPosition(false)
    setFilters({
      ...filters,
      ordering: 'label',
    })
    console.log('Unable to retrieve your location')
  }

  useEffect(() => {
    if (
      navigator.geolocation &&
      geoPermission !== 'denied' &&
      filters.ordering === ''
    ) {
      setIsLoadingPosition(true)
      navigator.geolocation.getCurrentPosition(success, error)
    } else {
      // console.log('Geolocation not supported')
      if (filters.ordering === '') {
        setFilters({
          ...filters,
          ordering: 'label',
        })
      }
    }
  }, [])

  const { t, i18n } = useTranslation()

  const { country } = usePaeseContext()
  const navigation = useNavigate()

  useEffect(() => {
    if(!country) {
      navigation('/' + i18n.language + '/')
    }
  }, [country])
    
  return (
    <div className={classNames(styles.ListMonuments)} ref={listMonumentsRef}>
      {(isFetching && !isFetchingNextPage) || isLoadingPosition || isLoading ? (
        <div className="d-flex align-items-center justify-content-center w-100 h-100">
          <div className="loader" />
        </div>
      ) : (
        infiniteMonuments &&
        infiniteMonuments?.pages.map((list, i) => (
          <Fragment key={i}>
            {list.results.length > 0 &&
              list.results.map((monument, k) => {
                return (
                  <div
                    key={k}
                    className={classNames({
                      [styles.DisabledEntry]: monument.disabled,
                      [styles.MonumentCard]: !detail || detail !== monument.id,
                      [styles.MonumentCardWithDetail]:
                        detail && detail === monument.id,
                    })}
                    onClick={() => {
                      setDetail(monument.id)
                      setFilters({
                        ...filters,
                        monument_id: monument.id,
                        monument_lat: monument.position?.coordinates[1],
                        monument_lon: monument.position?.coordinates[0],
                      })
                    }}
                  >
                    {monument.disabled && (
                      <div className={styles.DisabledEntryLabel}>
                        {t('DISABLED')}
                      </div>
                    )}
                    <div className="d-flex">
                      <div>
                        <IconMonument monument={monument} />
                      </div>
                      <div className="ms-2">
                        <div className={styles.MonumentTitle}>
                          {monument.label.charAt(0).toUpperCase() +
                            monument.label.slice(1)}
                        </div>
                        <div className={styles.City}>
                          {monument.municipality_label}
                          {monument.location &&
                            monument.location !==
                              monument.municipality_label && (
                              <div>Loc: {monument.location}</div>
                            )}
                        </div>
                      </div>
                    </div>
                    <div className="d-flex align-items-center flex-column">
                      <div className={styles.NumberPhoto}>
                        <div>{monument.pictures_count}</div>
                        <Camera className="ms-2" />
                      </div>
                      {monument.distance && geoPermission === 'granted' && (
                        <div className={styles.Distance}>
                          {monument.distance.toFixed(1)} km
                        </div>
                      )}
                    </div>
                  </div>
                )
              })}
            {list.results.length === 0 && (
              <div className={styles.NessunRisultato}>
                {t('NO_RESULTS_FOUND')}
              </div>
            )}
          </Fragment>
        ))
      )}
      {hasNextPage && !isLoading && (
        <Waypoint
          topOffset={-100}
          onEnter={() => {
            fetchNextPage()
          }}
        />
      )}
    </div>
  )
}

export default function List() {
  const { uiFilters, filters, setFilters, setFiltersDebounced } =
    useQsFilters(getFilters)
  const [detail, setDetail] = useState<number | null>(null)

  const { slug } = useParams()

  useEffect(() => {
    if (slug) {
      setDetail(Number(parseSmartSlug(slug)))
      setFilters({
        ...filters,
        search: getLabelFromSlug(slug),
      })
    }
  }, [slug])

  const navigate = useNavigate()

  const { i18n, t } = useTranslation()
  const location = useLocation()
  const currentPath = location.pathname

  return (
    <Layout>
      <div className="d-flex h-100 w-100">
        <div className="h-100">
          <BlockFilters
            filters={filters}
            setFilters={setFilters}
            setDetail={setDetail}
          />
        </div>
        <div className={styles.ListContainer}>
          <div
            className={styles.CardContainerList}
            style={{
              width: 'calc(100%)',
              height: '100%',
              transition: 'width 0.5s ease-in-out',
            }}
          >
            <div
              className={classNames({
                [styles.MonumentsBlock]: !detail,
                [styles.MonumentsBlockWithDetail]: detail,
              })}
            >
              <div className="w-100 position-relative d-flex align-items-center">
                {/* switch map or list */}
                <div className={styles.SwitchMapList}>
                  <Link
                    className={classNames({
                      [styles.SwitchItem]: true,
                    })}
                    to={`/${i18n.language}/mappa?${new URLSearchParams({
                      search: filters.municipality ? filters.search : '',
                      municipality: filters.municipality,
                      app_category: filters.app_category,
                      in_contest: filters.in_contest,
                      only_without_pictures: filters.only_without_pictures,
                      user_lat: filters.user_lat || '',
                      user_lon: filters.user_lon || '',
                      ordering: filters.ordering,
                      monument_id: filters.monument_id || '',
                      monument_lat: filters.monument_lat || '',
                      monument_lon: filters.monument_lon || '',
                      cluster_active: filters.cluster_active,
                      geo_context: filters.geo_context || String(getActiveGeocontext()?.id),
                      only_disabled: filters.only_disabled,
                      without_coordinates: filters.without_coordinates,
                    })}`}
                  >
                    {currentPath === '/' + i18n.language + '/' ||
                    currentPath === '/' ||
                    currentPath.indexOf('mappa') !== -1 ? (
                      <MapActive className="me-1" />
                    ) : (
                      <Map className="me-1" />
                    )}
                    {t('MAP')}
                  </Link>
                  <Link
                    to={`/${i18n.language}/lista?${new URLSearchParams({
                      search: filters.search,
                      municipality: filters.municipality,
                      app_category: filters.app_category,
                      in_contest: filters.in_contest,
                      only_without_pictures: filters.only_without_pictures,
                      user_lat: filters.user_lat || '',
                      user_lon: filters.user_lon || '',
                      ordering: filters.ordering,
                      monument_id: filters.monument_id || '',
                      monument_lat: filters.monument_lat || '',
                      monument_lon: filters.monument_lon || '',
                      cluster_active: filters.cluster_active,
                      geo_context: filters.geo_context || String(getActiveGeocontext()?.id),
                      only_disabled: filters.only_disabled,
                      without_coordinates: filters.without_coordinates
                    })}`}
                    className={classNames({
                      [styles.SwitchItemActive]: true,
                    })}
                  >
                    {currentPath === '/' + i18n.language + '/lista' ||
                    currentPath.indexOf('lista') !== -1 ? (
                      <ListIconActive className="me-1" />
                    ) : (
                      <ListIcon className="me-1" />
                    )}
                    {t('LIST')}
                  </Link>
                </div>
                <div className="position-relative w-100">
                  <input
                    onChange={(e) => {
                      setFiltersDebounced({ search: e.target.value })
                    }}
                    // disabled={isLoading}
                    value={uiFilters.search}
                    className={styles.InputSearch}
                    type="text"
                  />
                  <div className={styles.SearchIcon}>
                    <Search />
                  </div>
                </div>
                {filters.search && (
                  <div
                    className={styles.ClearSearch}
                    onClick={() => {
                      if (slug) {
                        navigate(
                          '/' +
                            i18n.language +
                            '/lista' +
                            '?search=&ordering=' +
                            filters.ordering +
                            '&municipality=' +
                            filters.municipality +
                            '&category=' +
                            filters.app_category +
                            '&in_contest=' +
                            filters.in_contest +
                            '&only_without_pictures=' +
                            filters.only_without_pictures +
                            '&without_coordinates' + 
                            filters.without_coordinates
                        )
                        setDetail(null)
                      } else {
                        setFilters({
                          ...filters,
                          search: '',
                        })
                      }
                    }}
                  >
                    <CloseSecondary />
                  </div>
                )}
              </div>
              <Suspense
                fallback={
                  <div
                    style={{
                      height: 'calc(100% - 64px)',
                    }}
                    className={
                      'w-100 d-flex align-items-center justify-content-center'
                    }
                  >
                    <div className="loader" />
                  </div>
                }
              >
                <ListMonuments
                  detail={detail}
                  setDetail={setDetail}
                  filters={filters}
                  setFilters={setFilters}
                />
              </Suspense>
            </div>
            <div className={'h-100 position-relative'}>
              {!detail && (
                <BlockOrdering filters={uiFilters} setFilters={setFilters} />
              )}
            </div>
          </div>
        </div>
        {!Number.isNaN(detail) && detail && (
          <Suspense
            fallback={
              <div className={styles.CardDetail}>
                <div className="w-100 h-100 d-flex align-items-center justify-content-center">
                  <div className="loader" />
                </div>
              </div>
            }
          >
            <div className={styles.CardDetail}>
              {!Number.isNaN(detail) &&
              <Detail isDesktop monumentId={detail} setDetail={setDetail} />
              }
            </div>
          </Suspense>
        )}
      </div>
    </Layout>
  )
}
